import { createAction } from 'redux-actions';

const ADD_INPUT_OPTIONS = 'ADD_INPUT_OPTIONS';

const addInputOptions = createAction(
  ADD_INPUT_OPTIONS,
  (text: string) => ({value: text})
)

export {
  ADD_INPUT_OPTIONS,
  addInputOptions
}