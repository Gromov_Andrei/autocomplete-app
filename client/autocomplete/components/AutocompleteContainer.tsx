import * as React from 'react';
import { connect } from 'react-redux';

import ViewInputForm from './ViewInputForm';
import ViewInputOptions from './ViewInputOptions';

import { addInputOptions } from '../actions';

/**
 * @prop {String} stateInputOptions Значение состояния списка ранее введеных значений.
 * @prop {String[]} filterData Отфильтрованный список ранее введеных значений.
 * @prop {String} value Значение в поле input.
 */
interface IState {
  stateInputOptions: string;
  filterData: string[];
  value: string;
}

/**
 * @prop {String[]} inputOptions Список ранее введеных значений.
 * @prop {Function} handleClickEnterInputForm Обработчик для добавление значения с поля input в список ранее введеных значений.
 */
interface IProps {
  inputOptions: string[];
  handleClickEnterInputForm: (e: any) => void;
}

class AutocompleteContainer extends React.Component<IProps, IState> {
  state = {
    stateInputOptions: 'none',
    value: '',
    filterData: []
  }
  
  /** Обработчик клика по полю input:
   * - изменение состояния для раскрытия списка ранее введеных значений.
   */
  handleClickInputForm = () => {
    if (this.state.stateInputOptions === 'none') {
      this.setState({stateInputOptions: 'block'});
    }
  }

  /** Обработчик вводимых данных в поле формы:
   * - фильтрация ранее введеных значений на основе вводимых данных.
   * @param {any} e Объект с данными о произошедшем событии.
   */
  handleChangeInput = (e: any) => {
    const value = e.target.value;
    const valueForFilter = value.toLowerCase();

    const filterData = this.props.inputOptions.filter((item) => (
      item.toLowerCase().indexOf(valueForFilter) > -1 
    ));

    this.setState({
      value: value,
      filterData: filterData
    });
  }

  /** Обработчик клика по пункту из списка ранее введеных значений
   * - добавление значения в поле формы
   * - скрытие списка ранее введеных значений
   * @param {any} e Объект с данными о произошедшем событии.
   */
  handleClickInputOptions = (e: any) => {
    this.setState({
      stateInputOptions: 'none',
      value: e.target.innerText,
      filterData: []
    });
  }

  /** Обработчик нажатия на клавишу ENTER в поле формы:
   * - добавление введеной информации в список ранее введеных значений.
   * @param {any} e Объект с данными о произошедшем событии.
   */
  handleClickEnterInput = (e: any) => {
    if (e.keyCode === 13) {
      if (e.target.value !== '') {
        this.props.handleClickEnterInputForm(e.target.value);
        this.setState({
          value: '',
          filterData: []
        });
      }

      e.preventDefault();
    }
  }

   render() {
     const listValue = this.state.filterData.length > 0 ? this.state.filterData : this.props.inputOptions;

    return (
      <section>
        <header>
          <h1>Autocomplete</h1>
        </header>
        <div id='autocomplete-container'>
          <ViewInputForm 
            valueInput={this.state.value} 
            clickInput={this.handleClickInputForm}
            enterInput={this.handleClickEnterInput}
            handleChange={this.handleChangeInput}
            />
          <ViewInputOptions 
            data={listValue}
            statusView={this.state.stateInputOptions}
            setInputValue={this.handleClickInputOptions}
          />
        </div>
      </section>
    )
  }
}

const mapStateToProps = (state) => ({
    inputOptions: state.autocomplete.listInputOptions
});

const mapDispatchToProps = (dispatch) => ({
  handleClickEnterInputForm: (value) => {
    dispatch(addInputOptions(value));
  }
});

 const ViewAutocompleteContainer = connect(
    mapStateToProps,
    mapDispatchToProps
  )(AutocompleteContainer)

export default ViewAutocompleteContainer;
