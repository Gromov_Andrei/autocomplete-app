import * as React from 'react';
import { FormGroup, ControlLabel, FormControl } from 'react-bootstrap';

/**
 * @prop {String} valueInput Данные для отображения в поле input.
 * @prop {Function} clickInput Изменение состояния для раскрытия списка ранее введеных значений.
 * @prop {Function} enterInput Добавление введеной информации в список ранее введеных значений.
 * @prop {Function} handleChange Обработчик вводимых данных в поле input.
 */
interface IProps {
  valueInput: string;
  clickInput: () => void;
  enterInput: (e: any) => void;
  handleChange: (e: any) => void;
}

class ViewInputForm extends React.Component<IProps, void> {
  render () {
    const { valueInput, clickInput, enterInput, handleChange } = this.props;

    return (
      <form autoComplete='off'>
        <FormGroup controlId='formControlsText' style={{marginBottom: 4}}>
          <ControlLabel>
            Вводим, смотрим. Смотрим, вводим...
          </ControlLabel>

          <FormControl
            type='text'
            value={valueInput}
            placeholder='Enter text'
            onChange={handleChange}
            onClick={clickInput}
            onKeyDown={enterInput}
          />
          
        </FormGroup> 
      </form>
    )
  }
}

export default ViewInputForm;
