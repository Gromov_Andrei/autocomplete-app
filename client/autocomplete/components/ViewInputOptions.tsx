import * as React from 'react';
import { ListGroup, ListGroupItem }  from 'react-bootstrap';

/**
 * @prop {String[]} data Данный для списка ранее введеных значений.
 * @prop {String} statusView Статус отображения списка ранее введеных значений.
 * @prop {Function} setInputValue Добавление значения в поле формы.
 */
interface IProps {
  data: string[];
  statusView: string;
  setInputValue: (e: any) => void;
}

class ViewInputOptions extends React.Component<IProps, void> {
  render () {
    const { data, statusView, setInputValue } = this.props;

    return (
      <ListGroup style={{display: statusView}}>
        {
          data.map((item) => {
            const keyValue = Math.random() * 1000
            return <ListGroupItem key={item + keyValue} onClick={setInputValue}>{item}</ListGroupItem>
          })
        }
        
      </ListGroup>
    )
  }
}

export default ViewInputOptions;
