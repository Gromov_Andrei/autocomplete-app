import { handleActions, Action } from 'redux-actions';
import { ADD_INPUT_OPTIONS } from './actions';

const dataTips = ['Маша', 'Петя', 'Вова', 'Билл'];

/**
 * @prop {String[]} listInputOptions Список ранее введеных значений.
 */
interface IState {
  listInputOptions: string[];
}

/**
 * @prop {String} value Значение для добавление в список ранее введеных значений
 */
interface IAction {
  value: string;
}

/** Значение состояния по умолчанию */
const initialState: IState = {
  listInputOptions: dataTips
}

/**
 * Добавление нового значения в список ранее введеных значений.
 * @param {String[]} oldData Прошлый список ранее введеных значений
 * @param {String} newValue Новое значение для добавление в список
 */
function setListInputOptions (oldData: string[], newValue: string) {
  let newData = oldData.concat();
  newData.push(newValue);

  return newData;
}

export default handleActions<IState>({
  [ADD_INPUT_OPTIONS]: (state: IState, action: Action<IAction>): IState => {
    return {
      listInputOptions: setListInputOptions(state.listInputOptions, action.payload.value)
    }
  }
}, initialState)
