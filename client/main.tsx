
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Store, createStore } from 'redux';
import { Provider } from 'react-redux';
 import { BrowserRouter as Router, Route } from "react-router-dom";

import RootPage from './main/components/RootPage'
import rootReducer from './main/reducer';

const initialState = {};

const store: Store<any> = createStore(rootReducer, initialState);

ReactDOM.render (
  <Provider store={store}>
    <Router>
      <Route path='/' component={RootPage} />
    </Router>
  </Provider>,
  document.getElementById('app')
)