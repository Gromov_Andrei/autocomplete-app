import * as React from 'react';
import AutocompleteContainer from '../../autocomplete/components/AutocompleteContainer';

class AutocompleteApp extends React.Component<any, void> {
  render() {
    return (
      <AutocompleteContainer />
    )
  }
}

export default AutocompleteApp;
