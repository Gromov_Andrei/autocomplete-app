import * as React from 'react';
import { Link, Route, Switch } from 'react-router-dom';

import AutocompletePage from './AutocompletePage';
import ToDoPage from './ToDoPage'

class RootPage extends React.Component<any, void> {
  render () {
    return (
      <div>
        <ul className='navigation'>
          <li><Link to ='/todo'>To do list</Link></li>
          <li><Link to ='/autocomplete'>Autocomplete field</Link></li>
        </ul>

        <Switch>
          <Route exact path='/' component={ToDoPage} />
          <Route path='/todo' component={ToDoPage} />
          <Route path='/autocomplete' component={AutocompletePage} />
        </Switch>
      </div>
    )
  }
}

export default RootPage;
