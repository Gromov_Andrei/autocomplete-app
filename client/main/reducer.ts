import { combineReducers } from 'redux';

import todos from '../todos';
import autocomplete from '../autocomplete/reducer';

const rootReducer = combineReducers({
  todos,
  autocomplete
});

export default rootReducer;
